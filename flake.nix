{
  description = "base flake";
  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";
  inputs.nixpkgs-20_09.url = "nixpkgs/nixos-20.09";
  outputs =
    { self
    , nixpkgs
    , nixpkgs-20_09
    }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
      config = pkgs.config;
      headless = import "${nixpkgs}/nixos/modules/profiles/headless.nix";
      pkgs-20_09 = import nixpkgs-20_09 { inherit system; };
    in
    {
      deployment = import ./src/base.nix
        { inherit config headless pkgs pkgs-20_09; } ;
      devShell.${system} = pkgs.mkShell {
        buildInputs = with pkgs; [
          nixpkgs-fmt
        ];
      };
    };
}
